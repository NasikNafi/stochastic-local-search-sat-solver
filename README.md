# Summary
This is a Stochastic Local Search(SLS) based sat solver similar to Walksat.


# Steps to build the code and use the solver

1. Build the code:

    `g++ -std=c++11 main.cpp -o solver`

2. Run the SAT solver:

    `./solver <input-filename> <max-flips> <max-tries> <noise>`
    
3. You should see the result in standard output.


# Test results on 100 benhmark problems

1. Test is conducted on 100 benchmark problems with timeout of 10 min, max flips 1000, max tries 500 and noise parameter 0.4. Among the benchmarks 60 are satisfiable and 40 are unsatisfiable. 


2. The file result.txt contains the result for the 100 benchamark problems located at the file 100-benchmarks.zip


3. The test is conducted on the follwing environment:

    MacBook Pro (OS X EI Capitan)
    
    Processor: 2.2 GHz Intel Core i7
    
    No of Logical cores: 4
    
    RAM: 16 GB 1600 MHz DDR3
    
    Graphics: Intel Iris Pro 1536 MB
