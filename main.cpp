#include <iostream>
#include <vector>
#include <fstream>
#include <sstream>
#include <stack>
#include <chrono>
#include <map>
#include <random>

using namespace std;

int solve();

int nVar, nClause;
vector<vector<int>> clauseList;
map<int, bool> curAssignment;
map<int, int> curBreakCount;

auto t1 = chrono::high_resolution_clock::now();

int read_input(char *fileName) {
    ifstream iFile(fileName);
    char temp[10];
    string str;

    // reading comments and parameters
    streampos oldpos;
    while (getline(iFile, str) && (str[0] == 'c' || str[0] == 'p')) {
        oldpos = iFile.tellg();
        if (str[0] == 'p') {
            string buf;
            stringstream ss(str);
            vector<string> tokens;
            while (ss >> buf)
                tokens.push_back(buf);
            nVar = stoi(tokens[tokens.size() - 2]);
            nClause = stoi(tokens[tokens.size() - 1]);
        }
    }

    // reading clauses
    iFile.seekg(oldpos);
    vector<int> singleClause;
    while (iFile >> temp) {
        // reading a clause with terminating 0
        if (temp[0] == '0') {
            if (singleClause.size() > 0) {
                clauseList.push_back(singleClause);
                singleClause.clear();
            }
        } else {
            int x = atoi(temp);
            singleClause.push_back(x);
        }
    }
    iFile.close();
    return 0;
}

int get_var(int x) {
    if (x < 0)
        return -x;
    else return x;
}

void print_result(bool result) {
    auto t2 = chrono::high_resolution_clock::now();
    chrono::duration<double, milli> elapsed = t2 - t1;
    cout << "c Given formula has " << nVar << " variables and " << nClause << " clauses." << endl;
    if (result) {
        cout << "s SATISFIABLE" << endl;
        cout << "v ";
        for (pair<int, bool> element : curAssignment) {
            if (element.first != 0) {
                if (element.second) {
                    cout << element.first << " ";
                } else {
                    cout << element.first * -1 << " ";
                }
            }
        }
        cout << "0" << endl;
    } else {
        cout << "s UNSATISFIABLE" << endl;
    }
    cout << "c Done (mycputime is " << elapsed.count() / 1000 << "s)." << endl;
}

bool check_formula() {
    int satClause = 0;
    for (int i = 0; i < clauseList.size(); ++i) {
        for (int j = 0; j < clauseList[i].size(); ++j) {

            map<int, bool>::iterator itr = curAssignment.find(get_var(clauseList[i][j]));
            int litStatus;
            if (itr->second == true) {
                litStatus = clauseList[i][j];
            } else {
                litStatus = clauseList[i][j] * -1;
            }
            if (litStatus > 0) {
                satClause++;
                break;
            }
        }
    }

    if (satClause == clauseList.size())
        return true;
    else
        return false;
}

bool is_unsatisfied(int clauseNo) {
    for (int i = 0; i < clauseList[clauseNo].size(); ++i) {
        map<int, bool>::iterator itr = curAssignment.find(get_var(clauseList[clauseNo][i]));
        if (itr != curAssignment.end()) {
            int litStatus;
            if (itr->second) {
                litStatus = clauseList[clauseNo][i];
            } else {
                litStatus = clauseList[clauseNo][i] * -1;
            }
            if (litStatus > 0)
                return false;
        }
    }
    return true;
}

void generate_random_assignment() {
    curAssignment.clear();
    for (int i = 1; i <= nVar; ++i) {
        if (rand() % 2)
            curAssignment.insert(pair<int, bool>(i, false));
        else
            curAssignment.insert(pair<int, bool>(i, true));
    }
}

int get_an_unsatisfied_clause_randomly() {
    vector<int> unsatClauses;
    for (int i = 0; i < clauseList.size(); ++i) {
        if (is_unsatisfied(i))
            unsatClauses.push_back(i);
    }

    int c = rand() % unsatClauses.size();
    return unsatClauses[c];
}

int get_breakcount_for_var(int var) {
    int breakCount = 0;
    for (int i = 0; i < clauseList.size(); ++i) {
        bool isResponsible = false;
        for (int j = 0; j < clauseList[i].size(); ++j) {
            int curVar = get_var(clauseList[i][j]);
            map<int, bool>::iterator itr = curAssignment.find(curVar);
            if (itr != curAssignment.end()) {
                int litStatus;
                if (itr->second) {
                    litStatus = clauseList[i][j];
                } else {
                    litStatus = clauseList[i][j] * -1;
                }
                if (litStatus > 0 && (var != curVar)) {
                    isResponsible = false;
                    break;
                } else if (litStatus > 0 && (var == curVar)) {
                    isResponsible = true;
                }
            }
        }
        if (isResponsible)
            breakCount++;
    }
    return breakCount;
}

// calculate break counts for all the variables of a clause defined by clauseNo
void calculate_breakcounts(int clauseNo) {
    curBreakCount.clear();
    for (int i = 0; i < clauseList[clauseNo].size(); ++i) {
        int breakCount = get_breakcount_for_var(get_var(clauseList[clauseNo][i]));
        curBreakCount.insert(pair<int, int>(get_var(clauseList[clauseNo][i]), breakCount));
    }
}

int get_min_breakcount_var() {
    int min = 100000, var = 0;
    for (map<int, int>::iterator it = curBreakCount.begin(); it != curBreakCount.end(); ++it) {
        if (it->second < min)
            var = it->first;
    }
    return var;
}

int get_random_var_from_clause(int clauseNo) {
    vector<int> varList;
    for (int i = 0; i < clauseList[clauseNo].size(); ++i) {
        varList.push_back(get_var(clauseList[clauseNo][i]));
    }

    int x = rand() % varList.size();
    return varList[x];
}

int solve(int maxFlip, double noiseParam) {
    generate_random_assignment();

    for (int i = 0; i < maxFlip; ++i) {
        if (check_formula())
            return 1;

        int flipVar;
        int c = get_an_unsatisfied_clause_randomly();
        calculate_breakcounts(c);

        int minBCVar = get_min_breakcount_var();
        int minBreakCount;
        map<int, int>::iterator itr = curBreakCount.find(minBCVar);
        if (itr != curBreakCount.end()) {
            minBreakCount = itr->second;
        }

        if (minBreakCount == 0) {
            flipVar = minBCVar;
        } else {
            double coin = rand() % 10 / 10;
            if (coin < noiseParam) {
                flipVar = get_random_var_from_clause(c);
            } else {
                flipVar = minBCVar;
            }
        }

        // fliping the assignment of flipVar
        map<int, bool>::iterator it = curAssignment.find(flipVar);
        if (it != curAssignment.end()) {
            if (it->second)
                it->second = false;
            else
                it->second = true;
        }
    }

    return 0;
}


int main(int argc, char *argv[]) {
    // getting command line argument
    char fileName[200];
    int MAX_FLIPS, MAX_TRIES;
    double NOISE;
    if (argc == 5) {
        strncpy(fileName, argv[1], 200);
        MAX_FLIPS = atoi(argv[2]);
        MAX_TRIES = atoi(argv[3]);
        NOISE = stod(argv[4]);
    } else {
        cout << "Provide required number of arguments correctly." << endl;
        return 0;
    }

    if (read_input(fileName) == -1) {
        return 0;
    }

    for (int i = 0; i < MAX_TRIES; ++i) {
        if (solve(MAX_FLIPS, NOISE) == 1) {
            print_result(true);
            return 0;
        }
    }
    print_result(false);
    return 0;
}